# Gamasutra Beautifier

![Gamasutra Logo](https://gitlab.com/stelabouras/gamasutra-beautifier/raw/master/icons/icon-128.png)

Gamasutra Beautifier is a tiny Google Chrome extension that tries to bring Gamasutra.com design to the 21st century.

This Chrome extension grew out of my frustration of browsing Gamasutra.com. 

The website holds a trove of valuable information for game developers and game development enthousiasts, hidden under an out-of-date design, which hinders readability.

I started applying some custom CSS rules in the article pages of Gamasutra.com which I expanded on the homepage and listing pages of the site.

This should not be considered by any way a suggested redesign, but just a way to make the site more readable.

In that sense, I haven't also removed any of the banners from the website.

## Changelog

Here's some changes that this extension applies to the original Gamasutra design:

* Full width articles.
* Improved code snippets.
* Improved home page design.
* Improved author pages.
* Better header design with a simplified version of Gamasutra logo.
* Full width embedded Youtube videos.
* Improved and high-res favicon.
* Better experience when clicking an outbound link (no more target attributes!).
* Button to return to the old design.

## Disclaimer

This extension and myself are by no means affiliated with Gamasutra.com.

## Install the Chrome extension

If you want to install the Chrome extension you can do that in the [Chrome web store](https://chrome.google.com/webstore/detail/lolfnkfliobkompoiflnlincaibobcfd/).
