chrome.pageAction.onClicked.addListener(() => {
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
      chrome.tabs.sendMessage(tabs[0].id, {'type': 'toggle_status'}, function(response) {
        console.log(response);
      });
    });    
});

chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {

    if (message.type == 'initialized') {

        chrome.pageAction.show(sender.tab.id);
    }
    else if(message.type == 'enabled') {

        chrome.pageAction.setTitle({
            'tabId' : sender.tab.id,
            'title' : 'Switch to original design'
        });
    }
    else if(message.type == 'disabled') {

        chrome.pageAction.setTitle({
            'tabId' : sender.tab.id,
            'title' : 'Switch to Gamasutra beautifier'
        });
    }

    sendResponse();
});   

chrome.webRequest.onBeforeRequest.addListener(
    function(details) {
        return { cancel: true };
    },
    {
        urls: [
            "https://epromos.ubmcanon.com/*.js",
            "https://ins.techweb.com/beacon/*.js"
        ]
	},
    [ "blocking" ]
);