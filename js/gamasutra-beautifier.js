if(!window.GamasutraBeautifier) {

    (function() {

      var GamasutraBeautifier = {

        initialize : function(recipient) {

          this.updateFavicon();

          Array
          .from(document.querySelectorAll('img[src$="hed.jpg"]'))
          .forEach((element) => {
            element.src = element.src.replace(/hed.jpg/, 'front.jpg');
            element.className = 'hero-image';
          });

          Array
          .from(document.querySelectorAll('img[src$="exclusive_btn.gif"]'))
          .forEach((element) => {
            element.parentElement.className += " exclusive";                        
          });

          Array
          .from(document.querySelectorAll('img[src$="editorIcon_gamaBlog.gif"]'))
          .forEach((element) => {
            element.parentElement.className += " editor";                        
          });

          Array
          .from(document.querySelectorAll('img[src$="featuredIcon_gamaBlog_dark.gif"]'))
          .forEach((element) => {
            element.parentElement.className += " featured";                        
          });

          Array
          .from(document.querySelectorAll('.page_item a'))
          .forEach((element) => {
            element.target = "";
          });

          Array
          .from(document.querySelectorAll('.stories_item'))
          .forEach((element) => {
            element.parentElement.className = "related";                        
          });          

          var footer = document.createElement('footer');

          var designSwitchElement = document.createElement('a');
          designSwitchElement.href = 'javascript:void(0);';
          designSwitchElement.id = 'gamasutra-beautifier-switch';
          designSwitchElement.innerText = 'Switch to original design';
          designSwitchElement.addEventListener('click', function() {
            document.body.className = "gamasutra-beautifier-disabled";

            document.querySelector('#gamasutra-beautifier-switch').style.display = 'none';

            window.scrollTo(0, 0);

            chrome.runtime.sendMessage({ 
              'type': 'disabled'
            }); 
          });

          var authElement = document.createElement('a');

          if(document.cookie.indexOf('GamaUser=') == -1) {

            authElement.href = 'https://www.gamasutra.com/sso/login.php';
            authElement.innerText = 'Login';
          }
          else {

            authElement.href = '/logout';
            authElement.innerText = 'Logout';
          }

          footer.appendChild(designSwitchElement);
          footer.appendChild(authElement);

          document.querySelector('div.container a[href="http://www.gamasutra.com"],div.container a[href="http://gamasutra.com"],div.container a[href="https://www.gamasutra.com"],div.container a[href="https://gamasutra.com"]').parentElement.appendChild(footer);

          chrome.runtime.sendMessage({ 
            'type': 'initialized'
          });          
        },

        updateFavicon: function() {

          var faviconURL = chrome.extension.getURL('icons/icon-256.png');

          // Set the generated favicon as the current one
          var link = document.createElement('link');
          link.type = 'image/x-icon';
          link.rel = 'shortcut icon';
          link.href = faviconURL;

          document.getElementsByTagName('head')[0].appendChild(link);
        },

        toggle: function() {          

          if(document.body.className.indexOf("gamasutra-beautifier-disabled") == 0) {
            document.body.className = "";
            document.querySelector('#gamasutra-beautifier-switch').style.display = 'block';

            chrome.runtime.sendMessage({ 
              'type': 'enabled'
            });  
          }
          else {
            document.body.className = "gamasutra-beautifier-disabled";
            document.querySelector('#gamasutra-beautifier-switch').style.display = 'none';

            chrome.runtime.sendMessage({ 
              'type': 'disabled'
            }); 
          }
        }
      };

      window.GamasutraBeautifier = GamasutraBeautifier;

    })(window);

    window.addEventListener('DOMContentLoaded', () => {

      GamasutraBeautifier.initialize();
    });

    chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {

        if (message.type == 'toggle_status') {

          GamasutraBeautifier.toggle();
        }

        sendResponse();
    });    
}